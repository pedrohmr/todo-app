import React, { FC, Dispatch, useEffect } from 'react';

import { useDispatch } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Layout from 'Components/Layout';
import AddList from 'Pages/AddList';
import AddTask from 'Pages/AddTask';
import List from 'Pages/List';
import {
  fetchLists as fetchListsAction,
  TListActionsType,
} from 'Store/actions/lists';

const App: FC<{}> = () => {
  const listsDispatch = useDispatch<Dispatch<TListActionsType>>();

  useEffect(() => {
    listsDispatch(fetchListsAction());
  }, []);

  return (
    <Router basename={process.env.PUBLIC_URL}>
      <Layout>
        <Switch>
          <Route exact path="/">
            <List />
          </Route>
          <Route path="/list/:id">
            <List />
          </Route>
          <Route path="/add-task">
            <AddTask />
          </Route>
          <Route path="/add-list">
            <AddList />
          </Route>
        </Switch>
      </Layout>
    </Router>
  );
};

export default App;
