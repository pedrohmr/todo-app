import { AxiosResponse } from 'axios';

import api from 'Utils/Api';

export const fetchLists = (): Promise<
  AxiosResponse<IListApi[] & IListApiError>
> => {
  return api.get(`lists`);
};

export const addList = (
  list: IList
): Promise<AxiosResponse<IList & IListApiError>> => {
  return api.post(`lists`, list);
};
