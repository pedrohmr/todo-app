import { AxiosResponse } from 'axios';

import api from 'Utils/Api';

export const addTask = (
  task: ITask
): Promise<AxiosResponse<ITask & ITaskApiError>> => {
  return api.post(`lists/${task.list_id}/tasks`, task);
};

export const toggleTask = (id: number): Promise<AxiosResponse<ITask>> => {
  return api.post(`tasks/${id}/toggle`);
};

export const deleteTask = (id: number): Promise<AxiosResponse<ITask>> => {
  return api.delete(`tasks/${id}`);
};
