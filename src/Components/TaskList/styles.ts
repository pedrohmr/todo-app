import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(
  ({ palette, spacing, shadows, shape, transitions }) => ({
    container: {
      marginBottom: spacing(4),
    },
    title: {
      color: palette.grey[400],
    },
    listItem: {
      backgroundColor: palette.common.white,
      borderRadius: shape.borderRadius,
      marginTop: spacing(1),
      transition: transitions.create(['box-shadow'], {
        duration: transitions.duration.standard,
        easing: transitions.easing.easeInOut,
      }),
      '&:hover': {
        backgroundColor: palette.common.white,
        boxShadow: shadows[5],
      },
    },
    listIcon: {
      minWidth: 0,
      marginRight: spacing(1),
    },
    listText: {
      marginLeft: spacing(1),
    },
    due_date: {
      marginRight: spacing(1),
    },
    note: {
      display: 'block',
      marginTop: spacing(1),
    },
    avatarContainer: {
      top: spacing(4),
    },
    avatarWrapper: {
      minWidth: 0,
    },
    avatar: {
      height: spacing(4),
      width: spacing(4),
    },
  })
);

export default useStyles;
