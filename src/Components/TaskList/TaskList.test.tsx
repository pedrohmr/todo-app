import React from 'react';

import { render } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';

import TaskList from './TaskList';

const mockCallback = jest.fn();

const mockProps = {
  title: 'Test title',
  tasks: [
    {
      id: 1,
      title: 'Test task 1',
      done: false,
      due_date: '2020-01-01',
      note: 'Test note 1',
      list_id: 1,
    },
    {
      id: 2,
      title: 'Test task 2',
      done: false,
      due_date: '2020-01-01',
      note: 'Test note 2',
      list_id: 1,
    },
  ],
  disabledElements: [],
  handleToggle: mockCallback,
  handleDelete: mockCallback,
};

const setup = (overwriteProps = {}) => {
  const { title, tasks, handleToggle, handleDelete } = {
    ...mockProps,
    ...overwriteProps,
  };

  return render(
    <Router>
      <TaskList
        title={title}
        tasks={tasks}
        handleToggle={handleToggle}
        handleDelete={handleDelete}
      />
    </Router>
  );
};

describe('When TaskList load', () => {
  test('should render the title', () => {
    const { getByText } = setup();
    expect(getByText(mockProps.title)).toBeInTheDocument();
  });

  test('should render the tasks', () => {
    const { getByText } = setup();
    mockProps.tasks.forEach(({ title }) => {
      expect(getByText(title)).toBeInTheDocument();
    });
  });

  test('should render the add button for no tasks', () => {
    const { getByTestId } = setup({ tasks: [] });
    expect(getByTestId('task-list-empty')).toBeInTheDocument();
  });
});

describe('When has iteraction with TaskList', () => {
  test('should trigger toggle handler on item click', () => {
    const { getByTestId } = setup();
    mockProps.tasks.forEach(({ id }) => {
      getByTestId(`task-list-item-${id}`).click();
      expect(mockProps.handleToggle).toBeCalled();
    });
  });

  test('should trigger delete handler on delete icon click', () => {
    const { getByTestId } = setup();
    mockProps.tasks.forEach(({ id }) => {
      getByTestId(`task-list-item-delete-${id}`).click();
      expect(mockProps.handleDelete).toBeCalled();
    });
  });
});
