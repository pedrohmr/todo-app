import React, { FC } from 'react';

import Avatar from '@material-ui/core/Avatar';
import Box from '@material-ui/core/Box';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import AddOutlinedIcon from '@material-ui/icons/AddOutlined';
import CheckCircleOutlinedIcon from '@material-ui/icons/CheckCircleOutlined';
import DeleteIcon from '@material-ui/icons/Delete';
import RadioButtonUncheckedOutlinedIcon from '@material-ui/icons/RadioButtonUncheckedOutlined';
import format from 'date-fns/format';
import faker from 'faker';
import { Link } from 'react-router-dom';

import useStyles from './styles';

interface ITaskListProps {
  title?: string;
  tasks?: ITask[];
  disabledElements?: number[];
  handleToggle?: (taskId: number) => void;
  handleDelete?: (task: ITask) => void;
}

const defaultProps: ITaskListProps = {
  title: '',
  tasks: [],
  disabledElements: [],
  handleToggle: () => undefined,
  handleDelete: () => undefined,
};

type TToggleEventHandler = (
  event: React.MouseEvent<HTMLDivElement>,
  id: number | undefined
) => void;

type TDeleteEventHandler = (
  event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  task: ITask
) => void;

const TaskList: FC<ITaskListProps> = ({
  title,
  tasks,
  disabledElements,
  handleToggle,
  handleDelete,
}) => {
  const styles = useStyles();

  const hasTasks = tasks && tasks.length > 0;

  const onToggle: TToggleEventHandler = (event, id) => {
    event.preventDefault();
    if (handleToggle && id) {
      handleToggle(id);
    }
  };

  const onDelete: TDeleteEventHandler = (event, task) => {
    event.preventDefault();
    event.stopPropagation();
    if (handleDelete) {
      handleDelete(task);
    }
  };

  const renderAvatar = () => {
    return (
      <ListItemAvatar className={styles.avatarWrapper}>
        <Avatar
          alt={faker.name.firstName()}
          src={faker.image.avatar()}
          className={styles.avatar}
        />
      </ListItemAvatar>
    );
  };

  const renderListItem = (task: ITask) => {
    const { id, title: taskTitle, note, done, due_date: dueDate } = task;

    if (!id) {
      return null;
    }

    const isUpdating = disabledElements?.includes(id);
    const primaryText = (
      <>
        <Chip
          size="small"
          className={styles.due_date}
          label={format(new Date(dueDate), 'P')}
        />
        {taskTitle}
      </>
    );
    const secondaryText = <span className={styles.note}>{note}</span>;

    return (
      <ListItem
        data-testid={`task-list-item-${id}`}
        onClick={e => !isUpdating && onToggle(e, id)}
        key={id}
        button
        className={styles.listItem}
      >
        <ListItemIcon className={styles.listIcon}>
          <Checkbox
            edge="start"
            tabIndex={-1}
            icon={<RadioButtonUncheckedOutlinedIcon />}
            checkedIcon={<CheckCircleOutlinedIcon />}
            color="primary"
            disabled={isUpdating}
            checked={done}
            disableRipple
          />
        </ListItemIcon>
        <ListItemIcon className={styles.listIcon}>
          <IconButton
            data-testid={`task-list-item-delete-${id}`}
            onClick={e => onDelete(e, task)}
            size="small"
            disabled={isUpdating}
            aria-label="delete"
          >
            <DeleteIcon />
          </IconButton>
        </ListItemIcon>
        <ListItemText
          primary={primaryText}
          secondary={secondaryText}
          className={styles.listText}
        />
        <ListItemSecondaryAction className={styles.avatarContainer}>
          {renderAvatar()}
        </ListItemSecondaryAction>
      </ListItem>
    );
  };

  return (
    <Box className={styles.container}>
      {title && (
        <Typography variant="subtitle2" className={styles.title}>
          {title}
        </Typography>
      )}
      <List>
        {tasks?.map(task => renderListItem(task))}
        {!hasTasks && (
          <ListItem
            data-testid="task-list-empty"
            button
            component={Link}
            to="/add-task"
            className={styles.listItem}
          >
            <ListItemIcon className={styles.listIcon}>
              <IconButton aria-label="delete">
                <AddOutlinedIcon />
              </IconButton>
            </ListItemIcon>
            <ListItemText>Add your first to-do in this list</ListItemText>
          </ListItem>
        )}
      </List>
    </Box>
  );
};

TaskList.defaultProps = defaultProps;

export default TaskList;
