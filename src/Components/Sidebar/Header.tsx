import React, { FC } from 'react';

import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import SvgIcon from '@material-ui/core/SvgIcon';
import TextField from '@material-ui/core/TextField';
import MenuIcon from '@material-ui/icons/Menu';

import { ReactComponent as PartlyCloudy } from 'Assets/icons/weather/PartlyCloudy.svg';

import useStyles from './styles';

const Header: FC<{}> = () => {
  const styles = useStyles();
  const iconSize = 128;

  return (
    <Grid container>
      <Grid item xs={6}>
        <IconButton aria-label="expand sidebar">
          <MenuIcon className={styles.icon} />
        </IconButton>
      </Grid>
      <Grid item xs={6}>
        <Box textAlign="right">
          <SvgIcon
            style={{ fontSize: 48 }}
            width={iconSize}
            height={iconSize}
            viewBox={`0 0 ${iconSize} ${iconSize}`}
            component={PartlyCloudy}
          />
        </Box>
      </Grid>
      <Grid item xs={12}>
        <Box marginTop={1}>
          <TextField
            placeholder="Search"
            variant="outlined"
            size="small"
            fullWidth
            InputProps={{
              classes: {
                root: styles.search,
                notchedOutline: styles.searchNotchedOutline,
              },
            }}
          />
        </Box>
      </Grid>
    </Grid>
  );
};

export default Header;
