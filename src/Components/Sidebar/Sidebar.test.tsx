import React from 'react';

import { render } from '@testing-library/react';
import axios from 'axios';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import store from 'Store';

import Sidebar from './Sidebar';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

const generateTask = (id: number, listId: number) => ({
  id,
  title: `Test task ${id}`,
  done: false,
  due_date: '2020-01-01',
  note: `Test note ${id}`,
  list_id: listId,
});

const generateList = (id: number, icon: string, main = false) => ({
  id,
  title: `Test list ${id}`,
  icon,
  metadata: main
    ? {
        default: true,
      }
    : null,
  tasks: [generateTask(id * 2 - 1, 2), generateTask(id * 2, 2)],
});

const mockDataResponse = {
  data: [
    generateList(1, 'InboxOutlined', true),
    generateList(2, 'PersonalVideoOutlined'),
    generateList(3, 'ShoppingCartOutlined'),
  ],
};

const setup = () => {
  return render(
    <Provider store={store}>
      <Router>
        <Sidebar />
      </Router>
    </Provider>
  );
};

describe('When Sidebar load', () => {
  test('should render the title', () => {
    const { getByTestId } = setup();
    expect(getByTestId('sidebar-skeleton')).toBeInTheDocument();
  });
});

// describe('When Data loads from API', () => {
// test('should render the title', async () => {
//   const { findByText } = setup();
//   mockedAxios.get.mockResolvedValueOnce(mockDataResponse);
//   expect(mockedAxios.get).toHaveBeenCalled();
//   const title = await findByText('Test list 1');
//   expect(title).toBeInTheDocument();
// });
// });
