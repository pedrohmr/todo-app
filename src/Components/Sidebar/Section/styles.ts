import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(({ palette, shape, spacing, transitions }) => ({
  listItem: {
    borderRadius: shape.borderRadius,
    '&.Mui-selected': {
      backgroundColor: palette.grey[100],
    },
    '& .MuiSvgIcon-root': {
      color: palette.grey[600],
      transition: transitions.create(['color'], {
        duration: transitions.duration.shortest,
        easing: transitions.easing.easeInOut,
      }),
    },
    '&:hover .MuiSvgIcon-root': {
      color: palette.common.black,
    },
  },
  itemIcon: {
    marginRight: spacing(2),
    minWidth: 0,
  },
}));

export default useStyles;
