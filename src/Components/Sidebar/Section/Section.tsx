import React, { FC } from 'react';

import List from '@material-ui/core/List';
import SvgIcon from '@material-ui/core/SvgIcon';
import * as Icons from '@material-ui/icons';

import Item from './Item';

type SvgIconComponent = typeof SvgIcon;

interface IAllIcons {
  [key: string]: SvgIconComponent;
}

const AllIcons: IAllIcons = {};
Object.entries(Icons).forEach(([key, value]) => {
  AllIcons[key] = value;
});

interface SectionProps {
  items: IList[];
}

const Section: FC<SectionProps> = ({ items }: SectionProps) => {
  return (
    <List disablePadding dense>
      {items.map(({ id, title, icon, tasksCount }: IList) => {
        const iconElem = (icon && AllIcons[icon]) || AllIcons.LabelOutlined;
        return (
          <Item
            key={id}
            url={`/list/${id}`}
            title={title}
            tasksCount={tasksCount}
            icon={React.createElement(iconElem)}
          />
        );
      })}
    </List>
  );
};

export default Section;
