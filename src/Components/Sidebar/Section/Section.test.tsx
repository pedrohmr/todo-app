import React from 'react';

import { render } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';

import Section from './Section';

const generateTask = (id: number, listId: number) => ({
  id,
  title: `Test task ${id}`,
  done: false,
  due_date: '2020-01-01',
  note: `Test note ${id}`,
  list_id: listId,
});

const generateList = (id: number, icon: string, main = false) => ({
  id,
  title: `Test list ${id}`,
  icon,
  metadata: main
    ? {
        default: true,
      }
    : null,
  tasks: [generateTask(id * 2 - 1, 2), generateTask(id * 2, 2)],
  tasksCount: 2,
});

const mockProps = {
  items: [
    generateList(1, 'InboxOutlined', true),
    generateList(2, 'PersonalVideoOutlined'),
    generateList(3, 'ShoppingCartOutlined'),
  ],
};

const setup = () => {
  const { items } = mockProps;

  return render(
    <Router>
      <Section items={items} />
    </Router>
  );
};

describe('When Section load', () => {
  test('should render the items', () => {
    const { getByText } = setup();
    mockProps.items.forEach(({ title }) => {
      expect(getByText(title)).toBeInTheDocument();
    });
  });
});
