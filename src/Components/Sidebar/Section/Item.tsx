import React, { FC } from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Link } from 'react-router-dom';

import useStyles from './styles';

interface IItemProps {
  url: string;
  title: string;
  tasksCount: number;
  icon: React.ReactElement;
}

const Item: FC<IItemProps> = ({ url, title, tasksCount, icon }: IItemProps) => {
  const styles = useStyles();
  const subtitle = `${tasksCount} Task${tasksCount !== 0 ? 's' : ''}`;

  return (
    <ListItem button component={Link} className={styles.listItem} to={url}>
      <ListItemIcon className={styles.itemIcon}>{icon}</ListItemIcon>
      <ListItemText primary={title} secondary={subtitle} />
    </ListItem>
  );
};

export default Item;
