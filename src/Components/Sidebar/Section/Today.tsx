import React, { FC } from 'react';

import List from '@material-ui/core/List';
import CalendarTodayOutlinedIcon from '@material-ui/icons/CalendarTodayOutlined';
import isToday from 'date-fns/isToday';
import { useSelector } from 'react-redux';

import { AppState } from 'Store/reducers';

import Item from './Item';

const Today: FC<{}> = () => {
  const { data } = useSelector(({ tasks }: AppState) => tasks);

  const todayTasks = data.filter(({ due_date }) => {
    return isToday(new Date(due_date));
  }).length;

  return (
    <List disablePadding dense>
      <Item
        url="/"
        title="Today"
        tasksCount={todayTasks}
        icon={<CalendarTodayOutlinedIcon />}
      />
    </List>
  );
};

export default Today;
