import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(({ palette, spacing }) => ({
  headerWrapper: {
    padding: spacing(2),
  },
  sectionWrapper: {
    padding: `0 ${spacing(2)}px`,
  },
  sectionDivider: {
    margin: `${spacing(2)}px 0`,
  },
  addButtonWrapper: {
    padding: `0 ${spacing(2)}px`,
    marginBottom: spacing(2),
  },
  icon: {
    color: palette.grey[600],
  },
  search: {
    backgroundColor: palette.grey[100],
  },
  searchNotchedOutline: {
    borderWidth: 0,
  },
}));

export default useStyles;
