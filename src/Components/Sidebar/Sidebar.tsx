import React, { FC } from 'react';

import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import AddIcon from '@material-ui/icons/Add';
import Skeleton from '@material-ui/lab/Skeleton';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import { AppState } from 'Store/reducers';

import Header from './Header';
import Section from './Section';
import Today from './Section/Today';
import useStyles from './styles';

const Sidebar: FC<{}> = () => {
  const styles = useStyles();

  const { data, fetching } = useSelector(({ lists }: AppState) => lists);

  if (fetching) {
    return (
      <Box data-testid="sidebar-skeleton" padding={2}>
        <Skeleton animation="wave" />
        <Skeleton animation="wave" />
        <Skeleton animation="wave" />
      </Box>
    );
  }

  const mainList = data.filter(({ main }) => main);
  const secondaryLists = data.filter(({ main }) => !main);

  return (
    <Box>
      <Box className={styles.headerWrapper}>
        <Header />
      </Box>
      <Box className={styles.sectionWrapper}>
        <Today />
      </Box>
      <Box className={styles.sectionWrapper}>
        <Section items={mainList} />
      </Box>
      <Divider className={styles.sectionDivider} />
      <Box className={styles.addButtonWrapper}>
        <Button
          startIcon={<AddIcon />}
          component={Link}
          size="small"
          to="/add-list"
          variant="outlined"
          fullWidth
        >
          Add List
        </Button>
      </Box>
      <Box className={styles.sectionWrapper}>
        <Section items={secondaryLists} />
      </Box>
    </Box>
  );
};

export default Sidebar;
