import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(({ palette, spacing }) => ({
  container: {
    height: '100vh',
  },
  sidebar: {
    backgroundColor: palette.common.white,
    width: '250px',
  },
  content: {
    flex: 1,
    textAlign: 'center',
  },
  loadingContainer: {
    height: '100vh',
    justifyContent: 'center',
    alignItems: 'center',
  },
  fab: {
    position: 'absolute',
    bottom: spacing(4),
    right: spacing(4),
  },
}));

export default useStyles;
