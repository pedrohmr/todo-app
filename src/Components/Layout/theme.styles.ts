import { createTheme } from '@material-ui/core';
import common from '@material-ui/core/colors/common';
import grey from '@material-ui/core/colors/grey';

const customGrey = {
  ...grey,
  100: '#f7f7f7',
  600: '#888888',
};

const theme = createTheme({
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '#root': {
          height: '100vh',
        },
      },
    },
    MuiOutlinedInput: {
      root: {
        backgroundColor: common.white,
        color: customGrey[700],
      },
    },
  },
  palette: {
    background: {
      default: customGrey[100],
    },
    grey: customGrey,
    primary: {
      main: common.black,
    },
  },
});

theme.shadows[5] =
  '0px 3px 5px -1px rgba(0,0,0,0.08),0px 5px 8px 0px rgba(0,0,0,0.06),0px 1px 14px 0px rgba(0,0,0,0.05)';

export default theme;
