import React, { FC } from 'react';

import CircularProgress from '@material-ui/core/CircularProgress';
import CssBaseline from '@material-ui/core/CssBaseline';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import { ThemeProvider } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import { AppState } from 'Store/reducers';

import Sidebar from '../Sidebar';
import useStyles from './styles';
import theme from './theme.styles';

const Layout: FC<{}> = ({ children }) => {
  const styles = useStyles();

  const { fetching } = useSelector(({ lists }: AppState) => lists);

  const renderLoading = () => (
    <Grid container className={styles.loadingContainer}>
      <CircularProgress />
    </Grid>
  );

  const renderContent = () => (
    <>
      {children}
      <Fab
        component={Link}
        to="/add-task"
        color="primary"
        aria-label="add"
        className={styles.fab}
      >
        <AddIcon />
      </Fab>
    </>
  );

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Grid container className={styles.container}>
        <Grid item className={styles.sidebar}>
          <Sidebar />
        </Grid>
        <Grid item className={styles.content}>
          {fetching ? renderLoading() : renderContent()}
        </Grid>
      </Grid>
    </ThemeProvider>
  );
};

export default Layout;
