import axios from 'axios';

const {
  REACT_APP_API_BASE_URL: baseURL,
  REACT_APP_API_EMAIL: apiEmail,
  REACT_APP_API_TOKEN: apiToken,
} = process.env;

const api = axios.create({
  baseURL,
  headers: { 'X-User-Email': apiEmail, 'X-User-Token': apiToken },
});

export default api;
