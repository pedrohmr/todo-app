import { TListsActions } from 'Store/actions/lists';
import * as actionTypes from 'Store/actions/listsTypes';

const initialState: IListState = {
  loading: false,
  fetching: true,
  errors: [],
  data: [],
};

export default function listsReducer(
  state = initialState,
  action: TListsActions
): IListState {
  switch (action.type) {
    case actionTypes.FETCH_LISTS_STARTED:
      return {
        ...state,
        fetching: true,
      };
    case actionTypes.FETCH_LISTS_SUCCESS:
      return {
        ...state,
        errors: [],
        fetching: false,
        data: action.payload.map(
          ({ metadata, tasks, ...list }: IListApi): IList => ({
            ...list,
            main: !!metadata.default,
            tasksCount: tasks.length,
          })
        ),
      };
    case actionTypes.FETCH_LISTS_FAILURE:
      return {
        ...state,
        data: [],
        fetching: false,
        errors: action.payload.errors,
      };

    case actionTypes.ADD_LIST_STARTED:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.ADD_LIST_SUCCESS:
      return {
        ...state,
        errors: [],
        loading: false,
        data: [
          ...state.data,
          { ...action.payload, main: false, tasksCount: 0 },
        ],
      };
    case actionTypes.ADD_LIST_FAILURE:
      return {
        ...state,
        loading: false,
        errors: action.payload.errors,
      };

    case actionTypes.INCREMENT_TASKS_COUNT:
      return {
        ...state,
        errors: [],
        loading: false,
        data: state.data.map((list: IList) => {
          if (list.id === action.payload) {
            return {
              ...list,
              tasksCount: list.tasksCount + 1,
            };
          }
          return list;
        }),
      };

    case actionTypes.DECREMENT_TASKS_COUNT:
      return {
        ...state,
        errors: [],
        loading: false,
        data: state.data.map((list: IList) => {
          if (list.id === action.payload) {
            return {
              ...list,
              tasksCount: list.tasksCount - 1,
            };
          }
          return list;
        }),
      };

    default:
      return state;
  }
}
