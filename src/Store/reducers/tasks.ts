import { TTasksActions } from 'Store/actions/tasks';
import * as actionTypes from 'Store/actions/tasksTypes';

const initialState: ITaskState = {
  loading: false,
  updating: [],
  errors: [],
  data: [],
};

export default function tasksReducer(
  state = initialState,
  action: TTasksActions
): ITaskState {
  switch (action.type) {
    case actionTypes.ADD_TASK_STARTED:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.ADD_TASK_SUCCESS:
      return {
        ...state,
        errors: [],
        loading: false,
        data: [...state.data, action.payload],
      };
    case actionTypes.ADD_TASK_FAILURE:
      return {
        ...state,
        loading: false,
        errors: action.payload.errors,
      };

    case actionTypes.FETCH_TASKS_SUCCESS:
      return {
        ...state,
        data: action.payload.map(({ tasks }) => tasks).flat(),
      };

    case actionTypes.TOGGLE_TASK_STARTED:
    case actionTypes.TOGGLE_TASK_FAILURE:
    case actionTypes.DELETE_TASK_STARTED:
    case actionTypes.DELETE_TASK_FAILURE:
      return {
        ...state,
        updating: [...state.updating, action.payload],
      };
    case actionTypes.TOGGLE_TASK_SUCCESS:
      return {
        ...state,
        updating: state.updating.filter(id => id !== action.payload.id),
        data: state.data.map(task => {
          if (task.id === action.payload.id) {
            return {
              ...task,
              done: action.payload.done,
            };
          }
          return task;
        }),
      };

    case actionTypes.DELETE_TASK_SUCCESS:
      return {
        ...state,
        updating: state.updating.filter(id => id !== action.payload),
        data: state.data.filter(({ id }) => id !== action.payload),
      };

    default:
      return state;
  }
}
