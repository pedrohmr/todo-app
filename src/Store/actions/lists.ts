import { ThunkDispatch, ThunkAction } from 'redux-thunk';

import {
  addList as addListService,
  fetchLists as fetchListsService,
} from 'Services/lists';

import * as actionTypes from './listsTypes';
import {
  IFetchTasksSuccessAction,
  IIncrementTasksCountAction,
  IDecrementTasksCountAction,
  fetchTasksSuccess,
} from './shared';

interface IFetchListsStartedAction {
  readonly type: typeof actionTypes.FETCH_LISTS_STARTED;
}

interface IFetchListsSuccessAction {
  readonly type: typeof actionTypes.FETCH_LISTS_SUCCESS;
  readonly payload: IListApi[];
}

interface IFetchListsFailureAction {
  readonly type: typeof actionTypes.FETCH_LISTS_FAILURE;
  readonly payload: IListApiError;
}

interface IAddListStartedAction {
  readonly type: typeof actionTypes.ADD_LIST_STARTED;
}

interface IAddListSuccessAction {
  readonly type: typeof actionTypes.ADD_LIST_SUCCESS;
  readonly payload: IList;
}

interface IAddListFailureAction {
  readonly type: typeof actionTypes.ADD_LIST_FAILURE;
  readonly payload: IListApiError;
}

export type TListsActions =
  | IFetchListsStartedAction
  | IFetchListsSuccessAction
  | IFetchListsFailureAction
  | IFetchTasksSuccessAction
  | IAddListStartedAction
  | IAddListSuccessAction
  | IAddListFailureAction
  | IIncrementTasksCountAction
  | IDecrementTasksCountAction;

export type TListDispatchType = ThunkDispatch<IListState, null, TListsActions>;
export type TListActionsType = ThunkAction<
  void,
  IListState,
  null,
  TListsActions
>;

const addListStarted = (): IAddListStartedAction => ({
  type: actionTypes.ADD_LIST_STARTED,
});

const addListSuccess = (list: IList): IAddListSuccessAction => ({
  type: actionTypes.ADD_LIST_SUCCESS,
  payload: list,
});

const addListFailure = (errors: IListApiError): IAddListFailureAction => ({
  type: actionTypes.ADD_LIST_FAILURE,
  payload: errors,
});

const fetchListsStarted = (): IFetchListsStartedAction => ({
  type: actionTypes.FETCH_LISTS_STARTED,
});

const fetchListsSuccess = (listsApi: IListApi[]): IFetchListsSuccessAction => ({
  type: actionTypes.FETCH_LISTS_SUCCESS,
  payload: listsApi,
});

const fetchListsFailure = (
  errors: IListApiError
): IFetchListsFailureAction => ({
  type: actionTypes.FETCH_LISTS_FAILURE,
  payload: errors,
});

export function addList(list: IList): TListActionsType {
  return (dispatch: TListDispatchType): void => {
    dispatch(addListStarted());
    addListService(list)
      .then(({ data }) => {
        if (data) {
          dispatch(addListSuccess(data));
        }
      })
      .catch(error => {
        dispatch(addListFailure(error.response.data));
      });
  };
}

export function fetchLists(): TListActionsType {
  return (dispatch: TListDispatchType): void => {
    dispatch(fetchListsStarted());
    fetchListsService()
      .then(({ data }) => {
        if (data) {
          dispatch(fetchListsSuccess(data));
          dispatch(fetchTasksSuccess(data));
        }
      })
      .catch(error => {
        dispatch(fetchListsFailure(error.response.data));
      });
  };
}
