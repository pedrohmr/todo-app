import * as actionListsTypes from './listsTypes';
import * as actionTasksTypes from './tasksTypes';

export interface IIncrementTasksCountAction {
  readonly type: typeof actionListsTypes.INCREMENT_TASKS_COUNT;
  readonly payload: number;
}

export interface IDecrementTasksCountAction {
  readonly type: typeof actionListsTypes.DECREMENT_TASKS_COUNT;
  readonly payload: number;
}

export interface IFetchTasksSuccessAction {
  readonly type: typeof actionTasksTypes.FETCH_TASKS_SUCCESS;
  readonly payload: IListApi[];
}

export const incrementTasksCount = (
  listId: number
): IIncrementTasksCountAction => ({
  type: actionListsTypes.INCREMENT_TASKS_COUNT,
  payload: listId,
});

export const decrementTasksCount = (
  listId: number
): IDecrementTasksCountAction => ({
  type: actionListsTypes.DECREMENT_TASKS_COUNT,
  payload: listId,
});

export const fetchTasksSuccess = (
  listsApi: IListApi[]
): IFetchTasksSuccessAction => ({
  type: actionTasksTypes.FETCH_TASKS_SUCCESS,
  payload: listsApi,
});
