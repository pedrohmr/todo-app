import { ThunkDispatch, ThunkAction } from 'redux-thunk';

import {
  addTask as addTaskService,
  toggleTask as toggleTaskService,
  deleteTask as deleteTaskService,
} from 'Services/tasks';

import {
  IIncrementTasksCountAction,
  IDecrementTasksCountAction,
  IFetchTasksSuccessAction,
  incrementTasksCount,
  decrementTasksCount,
} from './shared';
import * as actionTypes from './tasksTypes';

interface IAddTaskStartedAction {
  readonly type: typeof actionTypes.ADD_TASK_STARTED;
}

interface IAddTaskSuccessAction {
  readonly type: typeof actionTypes.ADD_TASK_SUCCESS;
  readonly payload: ITask;
}

interface IAddTaskFailureAction {
  readonly type: typeof actionTypes.ADD_TASK_FAILURE;
  readonly payload: ITaskApiError;
}

interface IToggleTaskStartedAction {
  readonly type: typeof actionTypes.TOGGLE_TASK_STARTED;
  readonly payload: number;
}

interface IToggleTaskSuccessAction {
  readonly type: typeof actionTypes.TOGGLE_TASK_SUCCESS;
  readonly payload: ITask;
}

interface IToggleTaskFailureAction {
  readonly type: typeof actionTypes.TOGGLE_TASK_FAILURE;
  readonly payload: number;
}

interface IDeleteTaskStartedAction {
  readonly type: typeof actionTypes.DELETE_TASK_STARTED;
  readonly payload: number;
}

interface IDeleteTaskSuccessAction {
  readonly type: typeof actionTypes.DELETE_TASK_SUCCESS;
  readonly payload: number;
}

interface IDeleteTaskFailureAction {
  readonly type: typeof actionTypes.DELETE_TASK_FAILURE;
  readonly payload: number;
}

export type TTasksActions =
  | IAddTaskStartedAction
  | IAddTaskSuccessAction
  | IAddTaskFailureAction
  | IFetchTasksSuccessAction
  | IIncrementTasksCountAction
  | IDecrementTasksCountAction
  | IToggleTaskStartedAction
  | IToggleTaskSuccessAction
  | IToggleTaskFailureAction
  | IDeleteTaskStartedAction
  | IDeleteTaskSuccessAction
  | IDeleteTaskFailureAction;

export type TTaskDispatchType = ThunkDispatch<ITaskState, null, TTasksActions>;
export type TTaskActionsType = ThunkAction<
  void,
  ITaskState,
  null,
  TTasksActions
>;

const addTaskStarted = (): IAddTaskStartedAction => ({
  type: actionTypes.ADD_TASK_STARTED,
});

const addTaskSuccess = (task: ITask): IAddTaskSuccessAction => ({
  type: actionTypes.ADD_TASK_SUCCESS,
  payload: task,
});

const addTaskFailure = (errors: ITaskApiError): IAddTaskFailureAction => ({
  type: actionTypes.ADD_TASK_FAILURE,
  payload: errors,
});

const toggleTaskStarted = (taskId: number): IToggleTaskStartedAction => ({
  type: actionTypes.TOGGLE_TASK_STARTED,
  payload: taskId,
});

const toggleTaskSuccess = (task: ITask): IToggleTaskSuccessAction => ({
  type: actionTypes.TOGGLE_TASK_SUCCESS,
  payload: task,
});

const toggleTaskFailure = (taskId: number): IToggleTaskFailureAction => ({
  type: actionTypes.TOGGLE_TASK_FAILURE,
  payload: taskId,
});

const deleteTaskStarted = (taskId: number): IDeleteTaskStartedAction => ({
  type: actionTypes.DELETE_TASK_STARTED,
  payload: taskId,
});

const deleteTaskSuccess = (taskId: number): IDeleteTaskSuccessAction => ({
  type: actionTypes.DELETE_TASK_SUCCESS,
  payload: taskId,
});

const deleteTaskFailure = (taskId: number): IDeleteTaskFailureAction => ({
  type: actionTypes.DELETE_TASK_FAILURE,
  payload: taskId,
});

export function addTask(task: ITask): TTaskActionsType {
  return (dispatch: TTaskDispatchType): void => {
    dispatch(addTaskStarted());
    addTaskService(task)
      .then(response => {
        const { data } = response;
        if (data) {
          dispatch(addTaskSuccess({ ...data, list_id: task.list_id }));
          dispatch(incrementTasksCount(task.list_id));
        }
      })
      .catch(error => {
        dispatch(addTaskFailure(error.response.data));
      });
  };
}

export function toggleTask(taskId: number): TTaskActionsType {
  return (dispatch: TTaskDispatchType): void => {
    dispatch(toggleTaskStarted(taskId));
    toggleTaskService(taskId)
      .then(response => {
        const { data } = response;
        if (data) {
          dispatch(toggleTaskSuccess(data));
        }
      })
      .catch(() => {
        dispatch(toggleTaskFailure(taskId));
      });
  };
}

export function deleteTask({ id, list_id }: ITask): TTaskActionsType {
  return (dispatch: TTaskDispatchType): void => {
    if (id) {
      dispatch(deleteTaskStarted(id));
      deleteTaskService(id)
        .then(() => {
          dispatch(deleteTaskSuccess(id));
          dispatch(decrementTasksCount(list_id));
        })
        .catch(() => {
          dispatch(deleteTaskFailure(id));
        });
    }
  };
}
