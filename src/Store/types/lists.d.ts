interface IListApiMetadata {
  default?: boolean;
}

interface IListApi {
  id: number;
  title: string;
  icon: string | null;
  metadata: IListApiMetadata;
  tasks: ITask[];
}

interface IListApiError {
  errors: string[];
}

interface IList {
  id: number;
  title: string;
  icon: string | null;
  main: boolean;
  tasksCount: number;
}

interface IListState {
  fetching: boolean;
  loading: boolean;
  data: IList[];
  errors: string[];
}
