interface ITask {
  id?: number;
  title: string;
  due_date: string;
  note: string;
  done?: boolean;
  list_id: number;
}

interface ITaskState {
  loading: boolean;
  updating: number[];
  data: ITask[];
  errors: string[];
}

interface ITaskApiError {
  errors: string[];
}
