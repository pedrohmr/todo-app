import React, { FC, Dispatch, useEffect, useState } from 'react';

import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import SaveIcon from '@material-ui/icons/Save';
import { Alert, AlertTitle } from '@material-ui/lab';
import { Controller, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';

import {
  addList as addListAction,
  TListActionsType,
} from 'Store/actions/lists';
import { AppState } from 'Store/reducers';

import useStyles from './styles';

const AddList: FC<{}> = () => {
  const styles = useStyles();
  const { handleSubmit, control, reset } = useForm<IList>();
  const listsDispatch = useDispatch<Dispatch<TListActionsType>>();
  const [redirect, setRedirect] = useState<number | null>(null);

  const {
    loading,
    errors,
    data: listsData,
  } = useSelector(({ lists }: AppState) => lists);

  useEffect(() => {
    if (!loading && errors.length === 0) {
      const lastItem = listsData?.at(-1);
      if (redirect === null || !lastItem) {
        setRedirect(0);
      } else {
        reset();
        setRedirect(lastItem.id);
      }
    }
  }, [loading, errors.length]);

  const onSubmit = (data: IList) => {
    listsDispatch(addListAction(data));
  };

  const onReset = () => {
    reset();
  };

  if (redirect) {
    return <Redirect to={`/list/${redirect}`} />;
  }

  return (
    <Container className={styles.container}>
      <Typography variant="h5">Add To-Do</Typography>
      <Box
        component="form"
        onSubmit={handleSubmit(onSubmit)}
        onReset={onReset}
        className={styles.form}
      >
        <Grid container spacing={2}>
          {errors.length > 0 && (
            <Grid item xs={12}>
              <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                {errors.map(error => (
                  <div key={error}>- {error}</div>
                ))}
              </Alert>
            </Grid>
          )}
          <Grid item xs={12}>
            <Controller
              name="title"
              control={control}
              rules={{ required: true }}
              render={({ field: { onChange, value } }) => (
                <TextField
                  onChange={onChange}
                  value={value || ''}
                  label="Title"
                  variant="outlined"
                  size="small"
                  fullWidth
                  required
                />
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="icon"
              control={control}
              rules={{ required: true }}
              render={({ field: { onChange, value } }) => (
                <TextField
                  size="small"
                  onChange={onChange}
                  value={value || ''}
                  label="Icon"
                  variant="outlined"
                  fullWidth
                  required
                />
              )}
            />
          </Grid>
          <Grid item xs={6}>
            <Button
              disabled={loading}
              variant="contained"
              color="primary"
              type="submit"
              fullWidth
              disableElevation
              startIcon={<SaveIcon />}
            >
              Save
            </Button>
          </Grid>
          <Grid item xs={6}>
            <Button
              component={Link}
              to="/"
              disabled={loading}
              variant="contained"
              type="reset"
              fullWidth
              disableElevation
            >
              Cancel
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
};

export default AddList;
