import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(({ spacing, breakpoints }) => ({
  container: {
    margin: `${spacing(2)}px 0`,
    textAlign: 'center',
  },
  form: {
    maxWidth: breakpoints.values.md / 3,
    margin: `${spacing(2)}px auto 0 auto`,
    textAlign: 'left',
  },
}));

export default useStyles;
