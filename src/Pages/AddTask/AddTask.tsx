import React, { FC, Dispatch, useEffect, useState } from 'react';

import DateFnsUtils from '@date-io/date-fns';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import SaveIcon from '@material-ui/icons/Save';
import { Alert, AlertTitle } from '@material-ui/lab';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { Controller, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';

import {
  addTask as addTaskAction,
  TTaskActionsType,
} from 'Store/actions/tasks';
import { AppState } from 'Store/reducers';

import useStyles from './styles';

const AddTask: FC<{}> = () => {
  const styles = useStyles();
  const { handleSubmit, control, reset } = useForm<ITask>();
  const tasksDispatch = useDispatch<Dispatch<TTaskActionsType>>();
  const [redirect, setRedirect] = useState<number | null>(null);

  const { data: listsData } = useSelector(({ lists }: AppState) => lists);
  const {
    loading,
    errors,
    data: tasksData,
  } = useSelector(({ tasks }: AppState) => tasks);

  useEffect(() => {
    if (!loading && errors.length === 0) {
      const lastItem = tasksData?.at(-1);
      if (redirect === null || !lastItem) {
        setRedirect(0);
      } else {
        reset();
        setRedirect(lastItem.list_id);
      }
    }
  }, [loading, errors.length]);

  const onSubmit = (data: ITask) => {
    tasksDispatch(addTaskAction(data));
  };

  const onReset = () => {
    reset();
  };

  if (redirect) {
    return <Redirect to={`/list/${redirect}`} />;
  }

  return (
    <Container className={styles.container}>
      <Typography variant="h5">Add To-Do</Typography>
      <Box
        component="form"
        onSubmit={handleSubmit(onSubmit)}
        onReset={onReset}
        className={styles.form}
      >
        <Grid container spacing={2}>
          {errors.length > 0 && (
            <Grid item xs={12}>
              <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                {errors.map(error => (
                  <div key={error}>- {error}</div>
                ))}
              </Alert>
            </Grid>
          )}
          <Grid item xs={12}>
            <Controller
              name="title"
              control={control}
              rules={{ required: true }}
              render={({ field: { onChange, value } }) => (
                <TextField
                  onChange={onChange}
                  value={value || ''}
                  label="Title"
                  variant="outlined"
                  size="small"
                  fullWidth
                  required
                />
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="note"
              control={control}
              rules={{ required: true }}
              render={({ field: { onChange, value } }) => (
                <TextField
                  size="small"
                  onChange={onChange}
                  value={value || ''}
                  label="Description"
                  variant="outlined"
                  fullWidth
                  required
                />
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <Controller
                name="due_date"
                control={control}
                rules={{ required: true }}
                render={({ field: { onChange, value } }) => (
                  <KeyboardDatePicker
                    inputVariant="outlined"
                    size="small"
                    variant="inline"
                    format="yyyy/dd/MM"
                    onChange={onChange}
                    value={value || null}
                    label="Due Date"
                    autoOk
                    disableToolbar
                    fullWidth
                    required
                  />
                )}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="list_id"
              control={control}
              rules={{ required: true }}
              render={({ field: { onChange, value } }) => (
                <FormControl size="small" variant="outlined" fullWidth>
                  <InputLabel htmlFor="form-list_id">List</InputLabel>
                  <Select
                    onChange={onChange}
                    value={value || ''}
                    label="List"
                    inputProps={{
                      name: 'list_id',
                      id: 'form-list_id',
                    }}
                    required
                  >
                    {listsData &&
                      listsData.map(({ id, title }) => (
                        <MenuItem key={id} value={id}>
                          {title}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
              )}
            />
          </Grid>
          <Grid item xs={6}>
            <Button
              disabled={loading}
              variant="contained"
              color="primary"
              type="submit"
              fullWidth
              disableElevation
              startIcon={<SaveIcon />}
            >
              Save
            </Button>
          </Grid>
          <Grid item xs={6}>
            <Button
              component={Link}
              to="/"
              disabled={loading}
              variant="contained"
              type="reset"
              fullWidth
              disableElevation
            >
              Cancel
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
};

export default AddTask;
