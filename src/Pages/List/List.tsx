import React, { FC } from 'react';

import Container from '@material-ui/core/Container';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { AppState } from 'Store/reducers';

import ListDetail from './ListDetail';
import useStyles from './styles';
import Today from './Today';

interface ListParams {
  id?: string;
}

const List: FC<{}> = () => {
  const styles = useStyles();
  const { id } = useParams<ListParams>();
  const { data } = useSelector(({ lists }: AppState) => lists);

  let list = null;
  if (id) {
    const listId = parseInt(id, 10);
    list = data?.find(({ id: _listId }) => _listId === listId);
  }

  return (
    <Container maxWidth="md" className={styles.container}>
      {list ? <ListDetail list={list} /> : <Today />}
    </Container>
  );
};

export default List;
