import React, { FC, Dispatch } from 'react';

import Container from '@material-ui/core/Container';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';
import { useDispatch, useSelector } from 'react-redux';

import TaskList from 'Components/TaskList/TaskList';
import {
  toggleTask as toggleTaskAction,
  deleteTask as deleteTaskAction,
  TTaskActionsType,
} from 'Store/actions/tasks';
import { AppState } from 'Store/reducers';

import Header from './Header';

interface ListDetailProps {
  list: IList;
}

type TTasksPerStatus = {
  today: ITask[];
  outdated: ITask[];
  upcoming: ITask[];
};

const ListDetail: FC<ListDetailProps> = ({ list }) => {
  const { data, updating } = useSelector(
    ({ tasks: _tasks }: AppState) => _tasks
  );
  const tasksDispatch = useDispatch<Dispatch<TTaskActionsType>>();

  const tasks = data?.filter(({ list_id }) => list_id === list.id) || [];

  if (!data) {
    return null;
  }

  const onToggle = (taskId: number) => {
    tasksDispatch(toggleTaskAction(taskId));
  };

  const onDelete = (task: ITask) => {
    tasksDispatch(deleteTaskAction(task));
  };

  const todayDate = new Date();

  const tasksPerStatusReducer = (acc: TTasksPerStatus, task: ITask) => {
    const days = differenceInCalendarDays(new Date(task.due_date), todayDate);
    switch (true) {
      case days === 0:
        return { ...acc, today: [...acc.today, task] };
      case days < 0:
        return { ...acc, outdated: [...acc.outdated, task] };
      default:
        return { ...acc, upcoming: [...acc.upcoming, task] };
    }
  };

  const tasksPerStatus: TTasksPerStatus = tasks.reduce(tasksPerStatusReducer, {
    today: [],
    outdated: [],
    upcoming: [],
  });

  const { outdated, today, upcoming } = tasksPerStatus;

  const renderTaskList = (_tasks: ITask[] = [], title = '') => {
    if (title && _tasks.length === 0) {
      return null;
    }

    return (
      <TaskList
        key={title}
        title={title}
        tasks={_tasks}
        handleToggle={onToggle}
        handleDelete={onDelete}
        disabledElements={updating}
      />
    );
  };

  return (
    <Container maxWidth="md">
      <Header title={list.title} />
      {renderTaskList(today, 'Today')}
      {renderTaskList(outdated, 'Outdated')}
      {renderTaskList(upcoming, 'Upcoming')}
      {!tasks.length && renderTaskList()}
    </Container>
  );
};

export default ListDetail;
