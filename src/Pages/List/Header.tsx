import React, { FC } from 'react';

import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import MoreVertOutlinedIcon from '@material-ui/icons/MoreVertOutlined';
import NotificationsNoneOutlinedIcon from '@material-ui/icons/NotificationsNoneOutlined';
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import faker from 'faker';

import useStyles from './styles';

interface HeaderProps {
  title: string;
}

const Header: FC<HeaderProps> = ({ title }) => {
  const styles = useStyles();

  const renderAvatarGroup = () => {
    return [0, 1, 2].map(avatar => (
      <Avatar
        key={avatar}
        alt="Avatar"
        src={faker.image.avatar()}
        className={styles.avatar}
      />
    ));
  };

  return (
    <Grid container className={styles.headerContainer}>
      <Grid item xs={6}>
        <Typography variant="h6">{title}</Typography>
        <Typography variant="subtitle2" className={styles.headerSubtitle}>
          {faker.lorem.sentence()}
        </Typography>
      </Grid>
      <Grid item xs={6}>
        <Grid container className={styles.actionsContainer}>
          <Grid item>
            <IconButton aria-label="expand sidebar">
              <NotificationsNoneOutlinedIcon className={styles.icon} />
            </IconButton>
          </Grid>
          <Grid item>
            <IconButton aria-label="expand sidebar">
              <MoreVertOutlinedIcon className={styles.icon} />
            </IconButton>
          </Grid>
          <Grid item>
            <AvatarGroup className={styles.avatarGroup}>
              {renderAvatarGroup()}
            </AvatarGroup>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default React.memo(Header);
