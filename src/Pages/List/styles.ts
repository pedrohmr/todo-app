import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(({ spacing, palette }) => ({
  container: {
    textAlign: 'left',
  },
  headerContainer: {
    margin: `${spacing(2)}px 0 ${spacing(4)}px 0`,
  },
  headerSubtitle: {
    color: palette.grey[400],
  },
  actionsContainer: {
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  icon: {
    color: palette.grey[600],
  },
  avatarGroup: {
    marginLeft: spacing(2),
  },
  avatar: {
    height: spacing(4),
    width: spacing(4),
    '& .MuiAvatar-img': {
      borderRadius: '50%',
    },
  },
}));

export default useStyles;
