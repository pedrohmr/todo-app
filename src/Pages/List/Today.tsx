import React, { FC, Dispatch } from 'react';

import Container from '@material-ui/core/Container';
import isToday from 'date-fns/isToday';
import { useDispatch, useSelector } from 'react-redux';

import TaskList from 'Components/TaskList';
import {
  toggleTask as toggleTaskAction,
  deleteTask as deleteTaskAction,
  TTaskActionsType,
} from 'Store/actions/tasks';
import { AppState } from 'Store/reducers';

import Header from './Header';

const Today: FC<{}> = () => {
  const { data, updating } = useSelector(({ tasks }: AppState) => tasks);
  const tasksDispatch = useDispatch<Dispatch<TTaskActionsType>>();

  const tasks = data.filter(({ due_date }) => {
    return isToday(new Date(due_date));
  });

  const onToggle = (taskId: number) => {
    tasksDispatch(toggleTaskAction(taskId));
  };

  const onDelete = (task: ITask) => {
    tasksDispatch(deleteTaskAction(task));
  };

  return (
    <Container maxWidth="md">
      <Header title="Today" />
      <TaskList
        tasks={tasks}
        handleToggle={onToggle}
        handleDelete={onDelete}
        disabledElements={updating}
      />
    </Container>
  );
};

export default Today;
