declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NODE_ENV: 'development' | 'production';
      REACT_APP_API_BASE_URL: string;
      REACT_APP_API_EMAIL: string;
      REACT_APP_API_TOKEN: string;
    }
  }
}

export {};
